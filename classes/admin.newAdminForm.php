<!-- Lage en ny admin, Registrer -->
<form method="post" class="form-horizontal" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
   <?php echo $this->adminMessage; ?>
  <div class="form-group">
    <label for="email" class="control-label col-sm-2">Email</label>
    <div class="col-sm-10">
      <input type="email" placeholder="Email" class="form-control" name="addEmail" value="<?php if(isset($_POST['addEmail'])){ echo $_POST['addEmail']; } ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="firstname" class="control-label col-sm-2">Fornavn</label>
    <div class="col-sm-10">
      <input type="text" placeholder="Firstname" class="form-control" name="addFirstname" value="<?php if(isset($_POST['addFirstname'])){ echo $_POST['addFirstname']; } ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="surname" class="control-label col-sm-2">Etternavn</label>
    <div class="col-sm-10">
      <input type="text" placeholder="Surname" class="form-control" name="addSurname" value="<?php if(isset($_POST['addSurname'])){ echo $_POST['addSurname']; } ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="password" class="control-label col-sm-2">Passord</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" name="password" placeholder="Password">
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-offset-2 col-md-9">
      <input type="submit" id="createAdmin" class="btn btn-default" value="Create Admin" />
    </div>
  </div>
</form>
