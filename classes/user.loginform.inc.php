<!-- 
Code for signin/signup form was copied from http://bootsnipp.com/snippets/featured/login-amp-signup-forms-in-panel 
Adapted for this project by Øivind Kolloen, January 2014
-->

<div class="container">    
  <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
    <div class="panel panel-info" >
      <div class="panel-heading">
		<div class="panel-title">Logg inn</div>
        <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Glemt passord?</a></div>
      </div>     

      <div style="padding-top:30px" class="panel-body" >

        <?php echo $alert; ?>
                            
        <form id="loginform" class="form-horizontal" role="form" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
                                    
          <div style="margin-bottom: 25px" class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input id="login-username" type="email" class="form-control" name="email" <?php echo $email; ?> placeholder="Email">                                        
          </div>
                                
          <div style="margin-bottom: 25px" class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <input id="login-password" type="password" class="form-control" name="password" placeholder="password">
          </div>
                                       
          <div class="input-group">
            <div class="checkbox">
              <label>
                <input id="login-remember" type="checkbox" name="remember" value="1"> Husk meg
              </label>
            </div>
          </div>

          <div style="margin-top:10px" class="form-group">
          <!-- Button -->
            <div class="col-sm-12">
              <input type="submit" id="btn-login" class="btn btn-success" value="Login"/>
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-12 control">
              <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                Ingen konto! 
                <a href="#" onClick="$('#loginbox').hide(); $('#signupbox').show()">
                Registrer deg her
                </a>
              </div>
            </div>
          </div>    
        </form>     
      </div>                     
    </div>  
  </div><!-- #/loginbox -->

  <div id="signupbox" style="display:none; margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
    <div class="panel panel-info">
      <div class="panel-heading">
        <div class="panel-title">Registrer deg</div>
        <div style="float:right; font-size: 85%; position: relative; top:-10px">
          <a id="signinlink" href="#" onclick="$('#signupbox').hide(); $('#loginbox').show()">Logg Inn</a>
        </div>
      </div>  
      <div class="panel-body" >
        <form id="signupform" class="form-horizontal" role="form" method="post" action="signin.php">
                                
        <?php echo $alert; ?>
                                                            
          <div class="form-group">
            <label for="email" class="col-md-3 control-label">Email</label>
            <div class="col-md-9">
              <input type="email" class="form-control" name="email" <?php echo $email; ?> placeholder="Email Address">
            </div>
          </div>
                                    
          <div class="form-group">
            <label for="firstname" class="col-md-3 control-label">Fornavn</label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="firstname" <?php echo $firstname; ?>placeholder="First Name">
            </div>
          </div>
          
          <div class="form-group">
            <label for="lastname" class="col-md-3 control-label">Etternavn</label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="lastname" <?php echo $lastname; ?>placeholder="Last Name">
            </div>
          </div>
                                
          <div class="form-group">
            <label for="password" class="col-md-3 control-label">Passord</label>
            <div class="col-md-9">
              <input type="password" class="form-control" name="passwd" placeholder="Password">
            </div>
          </div>
                                    
          <div class="form-group">
          <!-- Button -->                                        
            <div class="col-md-offset-3 col-md-9">
              <input type="submit" id="btn-signup" class="btn btn-info" value="Sign Up"/>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div><!-- #/signupbox -->
</div><!-- ./container -->