<!-- Redigert kopi av playlist.videoToPlaylistForm.php -->
 <form  method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" >
  <div class="form-group">
    <label for="My playlist(s)"></label>Mine spillelister
     <select class="form-control" id="editPlaylist" name="chosenPlaylist">
       <?php
       foreach($this->getPlaylists($this->uId) as $playlist) {
           echo "<option value='" . $playlist['pId'] ."'>" . $playlist['title'] . "</option>";
       }
       ?>
     </select>
   </div>
  <div class="form-group">
      <input type="submit" id="add" class="btn btn-default" value="Rediger spillelisten" />
  </div>
</form>
