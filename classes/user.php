<?php
/**
 * The User class handles user login/logout and maintaining of login status.
 * The class accepts a database handle in the constructor and assumes there is
 * a table called users with the columns : id, email, pwd, givenname and surename.
 * For creating new users it is also assumed that the id field is auto incrementet
 * and that the email field has an index that enforces unique email (username).
 *
 * @author imt2291
 *
 */
class User
{
	var $uid = -1;
  var $admin = -1;
	var $name = NULL;
	var $db;
	var $unknownUser = null;

	/**
	 * The constructor accepts an object of PDO that contains the database connection.
	 * It is also assumed that the session is started before this file get included.
	 * Also note that $_SESSION, $_GET and $_POST needs to be of type superglobal.
	 *
	 * The constructor checks for a $_GET variable of logout, if it exists the user
	 * is logget out.
	 *
	 * If $_POST['email'] is set a logon is attempted. The password should then be
	 * in $_POST['password'].
	 *
	 * if neither $_GET['logout'] nor $_POST['email'] is set then $_SESSION['uid']
	 * is checked. If this is set the user details (name of the user) is retrieved
	 * for the database. If no user with id $_SESSION['uid'] exists the session
	 * variable is cleared.
	 *
	 * @param PDO $db
	 */

	function User ($db)
	{
		$this->db = $db;	// Store a refence to the database connection, not needed now, but maybe in the future
		if (isset($_GET['logout']))											// Logging out
		{
			unset ($_SESSION['uid']);											// Clear the session variable
      if (isset($_COOKIE['uid']))										// Check if cookie exists
			{
        $persistent = explode(",", $_COOKIE['uid']);// Delete row in persistant_login table
        $sql = "DELETE FROM persistent_login WHERE uid=?"; // Good solution?
      	$sth = $db->prepare($sql);
        $sth->execute(array($persistent[0]));

        unset ($_COOKIE['uid']);   									// Clear the cookie variable
        setcookie('uid', '', time() - (14 * 24 * 60 * 60)); // Delete the cookie
      }
		}
		else if (isset($_POST['email']))								// Logging in
		{
			$sql = "SELECT id, password, givenname, surname, user_type FROM users WHERE email=?";
			$sth = $db->prepare($sql);
			$sth->execute (array ($_POST['email']));			// Get user info from the database
			if ($row = $sth->fetch(PDO::FETCH_ASSOC))			// If user exists
			{
																										// If correct password
				if (isset($_POST['password'])&&password_verify ($_POST['password'], $row['password']))
				{
					$_SESSION['uid'] = $row['id'];						// Store user id in session variable
					$this->uid = $row['id'];									// Store user id in object
          $this->admin = $row['user_type'];
					$this->name = array ('givenname'=>$row['givenname'], 'surname'=>$row['surname']);

          if (isset($_POST['remember']))						// Check if "remember me" is ticked of
					{																					// Create the cookie
            $series = md5($this->name['givenname'].(time ()));
            $token = md5($this->uid.(time()));
																										// Put new cookie
						setcookie('uid', $this->uid. "," .$series. "," . $token, time() + (14 * 24 * 60 * 60));
                      															// Insert values in db
            $sql = "INSERT INTO persistent_login (uid, series, token) VALUES (?, ?, ?)";
            $sth = $db->prepare ($sql);
            $sth->execute (array ($this->uid, $series, $token));
          }
					else
						$this->unknownUser = 'Ukjent brukernavn/passord';
        }
			else 																					// Bad password
					// Note, never say bad password, then you confirm the user exists
					$this->unknownUser = 'Ukjent brukernavn/passord';
		}
		else 																						// Unknow user
																										// Same as for bad password
				$this->unknownUser = 'Ukjent brukernavn/passord';
		}
		else if (isset($_SESSION['uid']))								// A user is logged in
		{
			$sql = "SELECT givenname, surname, user_type FROM users WHERE id=?";
			$sth = $db->prepare($sql);
			$sth->execute (array ($_SESSION['uid']));		// Find user information from the database

			if ($row = $sth->fetch())										// User found
			{
				$this->uid = $_SESSION['uid'];						// Store user id in object
        $this->admin = $row['user_type'];       	// Store admin type
				$this->name = array ('givenname'=>$row['givenname'], 'surname'=>$row['surname']);
			}
			else 																				// No such user
				unset ($_SESSION['uid']);									// Remove user id from session
		}
		else if (isset($_COOKIE['uid']))							// User presents a cookie
		{
      $persistent = explode(",", $_COOKIE['uid']);

			$sql = "SELECT uid, series, token FROM persistent_login WHERE uid=? AND series=?";
      $sth = $db->prepare($sql);
      $sth->execute(array($persistent[0], $persistent[1]));

      // 1. If the series identifier is present and the hash of the token matches
			//	the hash for that series identifier, the user is considered authenticated.
			//		A new token is generated, a new hash for the token is stored over the old record,
			//			and a new login cookie is issued to the user (it's okay to re-use the series identifier).
      if ($row = $sth->fetch(PDO::FETCH_ASSOC))
			{																						// Start a new session
        $_SESSION['uid'] = $row['uid'];
        $this->uid = $persistent[0];

        $series = $persistent[1];							// Update the table
        $token = md5($this->uid.(time()));
        setcookie('uid', $this->uid.",".$series.",".$token, time()+(14 * 24 * 60 * 60));

				$sql = "UPDATE persistent_login SET token=? where uid=? AND series=?";
        $sth1 = $this->db->prepare($sql);
        $sth1->execute (array ($token, $this->uid, $series));
      }
    }
	}

	/**
	 * Use this function to get the user id for the logged in user.
	 * Returns -1 if no user is logged in, or the id of the logged in user.
	 *
	 * @return long integer with user id, or -1 if no user is logged in.
	 */

	public function getUID()
	{  // returns ID for user
		return $this->uid;
	}

	/**
	 * Use this function to get the name of the user.
	 * Returns NULL if no user is logged in, an array with given name and
	 * surename of a user is logged in.
	 *
	 * @return array containing first and last name of user
	 */
	function getName()
	{  // Returns the name of the user
		return $this->name;
	}

	/**
	 * This method returns true if a user is logged in, false if no
	 * user is logged in.
	 *
	 * @return boolean value of true if a user is logged in, false if no user is logged in.
	 */
	function isLoggedIn()
	{  // Check to see if the user is logged in
		return ($this->uid > -1);										// return true if userid > -1
	}

  function isAdmin ()
	{  // Check to see if user is admin or not
    return ($this->admin > 0);
  }

	/**
	 * This method inserts the HTML code for the login form when it is called.
	 * Note that any login attempt detected by the constructor will affect
	 * the outcome of this method. If a successfull login has been performed
	 * this method return an empty string.
	 * If a failed login has been detected an alert box will be shown informing
	 * the user of this fact. The user name will also be filled in with information
	 * from the failed attempt.
	 *
	 * This method also uses the calling script as recipient in the action attribute
	 * of the form.
	 *
	 */
	function insertLoginForm ()
	{
		if ($this->isLoggedIn())													// User is logged in
			return;																					// Do not insert anything.

		$alert = "";									// Initialize alert and email to blank strings
		$email = 'value=""';
		$firstname = 'value=""';
		$lastname = 'value=""';

		if (isset($this->unknownUser))										// If failed login
		{															// Set alert and email to be used in the form
			$alert = '<div class="alert alert-danger" role="alert">
									<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
									<span class="sr-only">Error:</span> '.$this->unknownUser.
								'</div>';
			$email = "value='{$_POST['email']}' ";
		}
		if (isset($_POST['lastname']))
		{
			$lastname = 'value="'.$_POST['lastname'].'"';
			$firstname = 'value="'.$_POST['firstname'].'"';
		}
		require_once "user.loginform.inc.php";
	}

	/*Function recives the data that the user whises to register in the db.
	If any of the registration forms have been left empty, the user recives an error.
	The function returns 0 if no forms are left empty. */
	function validateReg ($uname, $pwd, $givenname, $surname)						// validates user registrations
	{
		$allThere=0;                                      //used to check if the user filled inn all of the forms during their registration.

		if($pwd=="")
		{																									//If no password was entered
				$allThere--;
				$this->unknownUser = 'Du må skrive et gyldig passord';
		}

		if($surname=="")																	//If no last name was entered
		{
				$allThere--;
				$this->unknownUser = 'Du må skrive et gyldig etternavn';
		}

		if($givenname=="")
		{																									//If no first name was entered
				$allThere--;
				$this->unknownUser = 'Du må skrive et gyldig fornavn';
		}

		if($uname=="")
		{																									//If no email was enterd.
				$allThere--;
				$this->unknownUser = 'Du må skrive en gyldig email';
		}
		return $allThere;
	}

/* Function to add a new user to the database. Takes username, password, givenname
*  and then surname and then tries to add the new user to DB.
*
*  If this fails, then the email address is taken. If it works, then you will be
*  given the message 'Success!'
*/

	function addUser ($uname, $pwd, $givenname, $surname)	// add a user
	{
		$sql = 'INSERT INTO users (email, password, givenname, surname) VALUES (?, ?, ?, ?)';
		$sth = $this->db->prepare ($sql);
		$sth->execute (array ($uname, password_hash($pwd, PASSWORD_DEFAULT), $givenname, $surname));

		if ($sth->rowCount()==0)														// if nothing has been inserted
		{
      $this->unknownUser = 'email address already registered!';
      return (array ('error'=>'error', 'description'=>'Denne mailen eksisterer allerede!'));
    }

		$this->uid = $this->db->lastInsertId();
    $_SESSION['uid'] = $this->uid;
    $this->name = array('givenname'=>$givenname, 'surname'=>$surname);

		return (array ('success'=>'Success!'));
	}
}
// Create an object of the User class, this also makes sure the constructor is called.
$user = new User($db);
