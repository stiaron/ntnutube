<?php
/**
 * Brukes for å håndtere videoer og spille de.
 */
class Video
{
  var $db;
  var $uId;                                        // userID
  var $owner = false;
  var $uploadStatus = false;
  var $fileErrors = array();
  var $videoInfo = null;

  function __construct($db, $uId)
  {
    $this->db = $db;
    $this->uId = $uId;
    if (isset($_POST['title'])&&isset($_POST['cancel']))
    {
      header("Location: index.php");
      die();
    }
    else if (isset($_POST['submit']))
    {
      $this->uploadVideo($uId, $_POST['title'], $_FILES['file'], $_POST['description'], $_FILES['captionFile'], $_POST['lang'], $_FILES['thumbnail']);
      $this->videoInfo = array('title'=>$_POST['title'],'description'=>$_POST['description']);
    }
    else if (isset($_GET['rating']))            // Check if rating is positive or negative
      $this->addRating($_GET['rating'], $this->uId, $_GET['v']);
      // Call method for like or dislike
  }

  function insertNewVideoForm()
  {
    require_once('video.newVideoForm.php');
  }

  function uploadVideo ($uId, $title, $videoArr, $description, $captions, $lang, $thumbnail)
  {
    $target = "videos/";
    $newFile = $this->renameFile($videoArr);
    $target_dir = $target . basename(($newFile));
    $newThumbnail = $this->renameFile($thumbnail);

    // Passed all checks
    if ($this->validateUploadForm($title, $description, $videoArr, $captions, $lang, $thumbnail))
    {
      if ($this->uploadThumbnail($thumbnail, $newThumbnail))
      {                                                    // Insert video in db
        $sql = 'INSERT INTO videos (uId, title, description, source, thumbnail) VALUES (?, ?, ?, ?, ?)';
        $sth = $this->db->prepare($sql);

        if ($sth->execute(array($uId, $title, $description, $newFile, $newThumbnail)))
        {
          $id = $this->db->lastInsertId();
          $this->uploadCaptions($captions, $lang, $id);
        }

        // Move uploaded file to folder
        if (move_uploaded_file($videoArr["tmp_name"], $target_dir))
          header('Location: index.php?videoID='.$id);
      }
    }
  }

  private function renameFile($file)
  {
    $temp = explode(".", $file['name']);
    $newFile = round(microtime(true)) . '.' . end($temp);
    return $newFile;
  }

  private function uploadThumbnail ($thumbnail, $newThumbnail)
  {
    $path = "thumbnail/";
    $target_dir = $path . basename($newThumbnail);

    if (move_uploaded_file($thumbnail['tmp_name'], $target_dir))
      return true;
    return false;
  }

  private function uploadCaptions ($captionsArr, $lang, $vId)
  {
    $path = "captions/";                                  // Upload directory
    $count = 0;

    // If methods working, but doesn't stop form from submitting
    foreach ($captionsArr['name'] as $file => $name)
    {
      if ($lang[$count] !== 'none')
      {
                                                          // Insert caption in db
        $sql = 'INSERT INTO transcripts (vId, language, source) VALUES (?,?,?)';
        $sth = $this->db->prepare($sql);
        $sth->execute(array($vId, $lang[$count], $captionsArr['name'][$file]));

        move_uploaded_file($captionsArr['tmp_name'][$file], $path.basename($captionsArr['name'][$file]));
      }
      $count++;
    }
  }
  // Check video extension
  private function validateUploadForm ($title, $description, $file, $captions, $lang, $thumbnail)
  {                                       // $extsArr = array("video/mp4", "video/quicktime", "video/ogg");
    $this->uploadStatus = true;

    if (is_uploaded_file($captions['tmp_name'][0]) || is_uploaded_file($captions['tmp_name'][1]))
      $this->uploadStatus = $this->validateCaptionFile($captions, $lang);

    // Check if the user has selected a language, but not uploaded a file
    if (($lang[0] !== 'none' && !is_uploaded_file($captions['tmp_name'][0])) || ($lang[1] !== 'none' && !is_uploaded_file($captions['tmp_name'][1])))
    {
      $this->fileErrors[] = "Du må velge en språkfil for språket.";
      $this->uploadStatus = false;
    }

    // Check if the uer has selected the same language
    if ( $lang[0] !== 'none' && $lang[1] !== 'none')
    {
      if ($lang[0] == $lang[1])
      {
          $this->fileErrors[] = "Du må velge en språkfil for språket.";
          $this->uploadStatus = false;
      }
    }

    // Check if input is empty
    if ($title == "" || $description == "" || $file['name'] == "" || $thumbnail['name'] == '')
    {
      $this->fileErrors[] = "Du fylte ikke ut alle påkrevde felter.";
      $this->uploadStatus = false;
    }

    // Check file extension
    if ($file['type'] != 'video/quicktime' && $file['type'] != 'video/mp4' && $file['type'] != 'video/ogg')
    {
      $this->fileErrors[] = "Beklager, bare .mp4 and .mov filer er tillatt.";
      $this->uploadStatus = false;
    }

    // Check if file size is kept under the limit
    if ($file['size'] > 12885760)                       // 28 MB (size in bytes)
    {
      $this->fileErrors[] = "Filen går over størrelsen 28MB!";
      $this->uploadStatus = false;
    }

    // Check file extension for thumbnail
    if ($thumbnail['type'] != 'image/jpeg' && $thumbnail['type'] != 'image/png')
    {
      $this->fileErrors[] = "Thumbnail bildet du valgte har feil extension.";
      $this->uploadStatus = false;
    }
    return $this->uploadStatus;
  }

  private function validateCaptionFile ($captions, $lang)
  {
    $maxFileSize = 12885760;
    $validFormats = array("vtt");
    $bool = true;
    $num = 0;

    foreach ($captions['name'] as $file => $name)
    {
      if (is_uploaded_file($captions['tmp_name'][$file]))
      {
        if ($captions['error'][$file] > 0)
        {
          $bool = false;
          $this->fileErrors[] = "Det var problemer med filen du valgte.";
        }

        if ($captions['size'][$file] > $maxFileSize)
        {
          $bool = false;
          $this->fileErrors[] = "Språkfilen er for stor.";
        }

        if (!in_array(pathinfo($name, PATHINFO_EXTENSION), $validFormats))
        {
          $bool = false;
          $this->fileErrors[] = "Bare språkfiler med extension 'vtt' er tillatt.";
        }

        if ($lang[$num] == 'none')
        {
          $bool = false;
          $this->fileErrors[] = "Du må velge et språk.";
        }
      }
      $num++;
    }
    return $bool;
  }

  function getFileErrors ()
  {
    return $this->fileErrors;
  }

  function insertMyVideos ($uId)                    // Vis video
  {
    $sql = "SELECT vId, title, description, source, ts, thumbnail FROM videos WHERE uId=? ORDER BY ts DESC";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($uId));
    $videos = $sth->fetchAll(PDO::FETCH_ASSOC);
    return $videos;
  }

  function insertNewVideos ()                       // Legg ny video
  {
    $sql = "SELECT vId, title, description, source, ts, thumbnail FROM videos ORDER BY ts DESC";
    $sth = $this->db->prepare($sql);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
  }

  function getVideoInfo()
  {
    return $this->videoInfo;
  }

  function getCaptions ($vId)                         // Not ready
  {
    $sql = "SELECT tId, language, source FROM transcripts WHERE vId=?";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($vId));
    return $sth->fetchAll(PDO::FETCH_ASSOC);
  }

  function getVideo ($vId)                            // Hent video
  {
    $sql = "SELECT vId, title, description, source, ts, thumbnail FROM videos WHERE vId=?";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($vId));
    return $sth->fetch(PDO::FETCH_ASSOC);
  }

/*
funksjonen under legger til en vurdering på video. sjekk følgene:
-- sjekk om har rating fra før ?
  * hent hans rating fra DB -> tabell rating
  ** er det lik med den som han valgte nå
      ikke gjør noe dritt

  ** ikke lik ?
      oppdater ratings verdi til denne brukeren i tabellen rating

-- Har han ikke rating fra før ?
  * insert hans verdi i rating tabell med videoID og UserID
*/
  function addRating ($nowRating,$uId, $vId) // Legg til en vurdering av video
  {
    //Se om han har rating i Database:
    $sql = "SELECT rating FROM rating WHERE uId=? AND vId=?";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($uId, $vId));
    $mama = $sth -> fetch(PDO::FETCH_ASSOC);

    if($sth->rowCount() > 0)                    // Finner du now i DB?
    {
      if($mama["rating"]!=$nowRating)         // Annen rating enn før
      {
        $sql = "UPDATE rating SET rating=? WHERE uId=? AND vId=?";
        $sth = $this->db->prepare($sql);
        $sth->execute(array($nowRating, $uId, $vId));
        header('Location: watch.php?v='.$vId.'&rating='.$nowRating.'&success=true');
      }
    }
    else                                       // Første gangsrating
    {
      // nsert hans verdi i rating tabell med videoID og UserID
      $sql = "INSERT INTO rating(uId, vId, rating) VALUES (?,?,?)";
      $sth = $this->db->prepare($sql);
      $sth->execute(array($uId, $vId, $nowRating));
      header('Location: watch.php?v='.$vId.'&rating='.$nowRating.'&success=true');
    }
  }

  /*
  funksjonen under skal vise rating resultat på hver video, dette skjer slik:
  ved hjelp av Select avg for alle rating til videoId
  */
  function getAverageRating ($vId)
  {
    $sql = "SELECT AVG(rating) FROM rating WHERE vId=?";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($vId));
    $row = $sth->fetch();
    return $row[0];
  }

  function getPopVid()                              // Hent populare videoer
  {
    $sql = "SELECT rating.vId, title, thumbnail, ts, AVG(rating) FROM rating
      	INNER JOIN videos ON videos.vId = rating.vId GROUP BY vId ORDER BY AVG(rating) DESC
          LIMIT 10";
    $sth = $this->db->prepare($sql);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
  }

  function videoResult ($search)                    // Søke funksjonen
  {
    $sql = "SELECT vId, title, description, ts FROM videos WHERE title LIKE '%" . $search . "%' OR description LIKE '%" . $search ."%' LIMIT 5";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($search));
    if ( $sth->rowCount() > 0 )
      return $sth->fetchAll(PDO::FETCH_ASSOC);
    else
      return false;
  }

  function listVideoResults ($search)
  {
    // Check if query for video is found
    if (!$this->videoResult($search))
      echo "<div class='alert alert-danger' role='alert'>No video results found.</div>";
    else
    {
      echo "<h2>Video Results</h2>";
      foreach ($this->videoResult($search) as $result)
      {
        echo "<h3><a href='watch.php?v=".$result['vId']."'>".$result['title']."</a></h3>";
        echo "<p>".$result['description']."</p>";
      }
    }
  }
}                                                       // video class end

$video = new Video($db, $user->getUID());
