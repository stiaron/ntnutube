<!--Henter ut skjema-->
<form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
  <table class="table table-striped">
    <thead>
      <tr>
        <th><input id="checkAll" name="selectAll" value="1" type="checkbox"></th>
        <th>Full Name <a href="<?php if (isset($_GET['sort']) && $_GET['sort'] == 'name') {
            echo $_SERVER['PHP_SELF']; ?>"><i class="fa fa-sort-desc" aria-hidden="true"></i></a>
        <?php } else {
          echo $_SERVER['PHP_SELF']; ?>?sort=name"><i class="fa fa-sort-asc" aria-hidden="true"></i></a>
        <?php } ?>
        </th>
        <th>Videos <a href="<?php if (isset($_GET['sort']) && $_GET['sort'] == 'videos') {
          echo $_SERVER['PHP_SELF']; ?>"><i class="fa fa-sort-desc" aria-hidden="true"></i></a>
        <?php } else {
          echo $_SERVER['PHP_SELF']; ?>?sort=videos"><i class="fa fa-sort-asc" aria-hidden="true"></i></a>
        <?php } ?>
          </th>
        <th>Playlists <a href="<?php if (isset($_GET['sort']) && $_GET['sort'] == 'playlists') {
          echo $_SERVER['PHP_SELF']; ?>"><i class="fa fa-sort-desc" aria-hidden="true"></i></a>
          <?php } else {
          echo $_SERVER['PHP_SELF']; ?>?sort=playlists"><i class="fa fa-sort-asc" aria-hidden="true"></i></a>
          <?php } ?>
        </th>
        <th></th>
      </tr>
    </thead>
    <tbody>
     <?php
      if (isset($_GET['sort']) && $_GET['sort'] == 'name') {
         foreach ($this->sortUsersByName() as $row) { ?>
          <tr>
            <td><input name="checkbox[]" type="checkbox" value="<?php echo $row['id']; ?>" </td>
            <td><?php echo $row['givenname'] . " " . $row['surname']; ?></td>
            <td><?php echo $row['totalVideos'] ?></td>
            <td><?php echo $row['totalPlaylists'] ?></td>
            <td></td>
            <td><button type="submit" class="my-btn" name="deleteId" value="<?php echo $row['id']; ?>"><i class="fa fa-times fa-lg" aria-hidden="true"></i></button></td>
          </tr>
      <?php }
      } else if (isset($_GET['sort']) && $_GET['sort'] == 'videos') {
        foreach ($this->sortByVideos() as $row) { ?>
          <tr>
            <td><input name="checkbox[]" type="checkbox" value="<?php echo $row['id']; ?>" </td>
            <td><?php echo $row['givenname'] . " " . $row['surname']; ?></td>
            <td><?php echo $row['totalVideos'] ?></td>
            <td><?php echo $row['totalPlaylists'] ?></td>
            <td></td>
            <td><button type="submit" class="my-btn" name="deleteId" value="<?php echo $row['id']; ?>"><i class="fa fa-times fa-lg" aria-hidden="true"></i></button></td>
          </tr>
      <?php }
      } else if (isset($_GET['sort']) && $_GET['sort'] == 'playlists') {
        foreach ($this->sortByPlaylists() as $row) { ?>
          <tr>
            <td><input name="checkbox[]" type="checkbox" value="<?php echo $row['id']; ?>" </td>
            <td><?php echo $row['givenname'] . " " . $row['surname']; ?></td>
            <td><?php echo $row['totalVideos'] ?></td>
            <td><?php echo $row['totalPlaylists'] ?></td>
            <td></td>
            <td><button type="submit" class="my-btn" name="deleteId" value="<?php echo $row['id']; ?>"><i class="fa fa-times fa-lg" aria-hidden="true"></i></button></td>
          </tr>
      <?php }
      } else {
          foreach ($rows as $row) { ?>
          <tr>
            <td><input name="checkbox[]" type="checkbox" value="<?php echo $row['id']; ?>" </td>
            <td><?php echo $row['givenname'] . " " . $row['surname']; ?></td>
            <td><?php echo $row['totalVideos'] ?></td>
            <td><?php echo $row['totalPlaylists'] ?></td>
            <td></td>
            <td><button type="submit" class="my-btn" name="deleteId" value="<?php echo $row['id']; ?>"><i class="fa fa-times fa-lg" aria-hidden="true"></i></button></td>
          </tr>
      <?php }
      } ?>
    </tbody>
  </table>
  <input class="btn btn-default" type="submit" value="Remove"/>
</form>
