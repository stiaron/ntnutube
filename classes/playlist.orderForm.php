<?php if (count($this->getVideosInPlaylistSorted($_GET["chosenID"])) > 1) { ?>
  <form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
    <?php foreach ($this->getVideosInPlaylistSorted($_GET["chosenID"]) as $row) { /*Prints all videoes*/ ?>
      <div class="form-group">
        <div class="row">
          <div class="col-md-4">
            <a href="#"><?php echo $row['title'] ?></a>
          </div>
          <div class="col-md-6">
            <select name="changeOrder[]"> <!--Position-->
              <?php foreach($this->getVideosInPlaylistSorted($_GET['chosenID']) as $tull) {   //Creates a dorp-down for each of the videos
                echo "<option value='" . $tull['position'] ."'".  (($row['title'] == $tull['title']) ? "selected" : "") .">" . $tull['title'] . "</option>";
              } ?>
            </select>
            <input type="hidden" name="myArr[]" value="<?php echo $row['vId']; ?>">
          </div>
        </div>
      </div>
    <?php } ?>
    <input class="btn btn-default" type="submit" value="Bytt plass på videoene"/>
  </form>
<?php } else {
  echo "<h4>You need 2 or more videos.</h4>";
} ?>