<!-- Hente tabell som viser oversikt over alle admin-->
<table class="table table-striped">
  <thead>
    <tr>
      <th>Navn</th>
      <th>Email</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($admins as $admin) { ?>
      <tr>
        <td><?php echo $admin['givenname'] . " " . $admin['surname']; ?></td>
        <td><?php echo $admin['email']; ?></td>
      </tr>
    <?php } ?>
  </tbody>
</table>