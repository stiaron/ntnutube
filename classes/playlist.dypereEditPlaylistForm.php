<form id="playlistform" role="form" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
  <div class="form-group">
      <label for="title">Tittel</label>
      <input type="text" class="form-control" name="editTitle" id="title" placeholder="Tittel på spillelisten" value="<?php echo isset($_POST['editTitle']) ? $_POST['editTitle'] : ""; ?>">
  </div>
  <div class="form-group">
      <label for="title">Beskrivelse</label>
      <textarea class="form-control" name="editDescription" style="height: 150px;" placeholder="Beskrivelse av spillelisten"><?php echo isset($_POST['editDescription']) ? $_POST['editDescription'] : ""; ?></textarea>
  </div>
  <div style="margin-top:10px" class="form-group">
  <!-- Button -->
    <div class="col-sm-12">
      <input type="submit" id="btn-submit" class="btn btn-success" value="Rediger"/>
    </div>
  </div>
</form>