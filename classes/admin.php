<?php

/**
 * The Admin class handles admin privilegies and maintanance of other users.
 * The class accepts a database handle in the constructor. There is methods
 * for adding new admins and delete users.
 *
 */

/* extend user class */

class Admin {
  var $db;
  var $adminMessage;
  
  /**
	 * The constructor accepts an object of PDO that contains the db-connection.
     * 
     * The constructor checks if the $_POST variables for different admin actions is set.
     * If this is the case, call the corresponding method.
     *
     * @param PDO $db
     *
	 */
  
  function Admin ($db) {
    $this->db = $db;
    if (isset($_POST['deleteId'])) { // Delete one row
      $this->deleteUser($_POST['deleteId']);
    } else if (isset($_POST['checkbox'])) { // Delete multiple rows
      $this->deleteCheckedUsers($_POST['checkbox']);
    } else if (isset($_POST['addFirstname'])) { // Add new admin
      $this->addAdmin($_POST['addEmail'], $_POST['password'], $_POST['addFirstname'], $_POST['addSurname']);
    } else if (isset($_POST['userToPromote'])) {
      $this->promoteUser($_POST['userToPromote']);
    }
  }
  
  // REMOVE ADMINFORM?
  function insertAddAdminForm() {
    require('admin.newAdminForm.php');
  }
  
  function insertDeleteUserForm() {
    $rows = $this->completeUserInfo();
    require('admin.deleteUsersForm.php');
  }
  
  function insertPromoteForm() {
    $members = $this->getUser(0);
    require('admin.promoteUserForm.php');
  }
  
  function insertAdminList () {
    $admins = $this->getUser(1);
    require('admin.adminList.php');
  }
  
  // REMOVE VALIDATOR?
  private function validateAdmin ($uname, $givenname, $surname, $pwd) {
    $errors = [];
    if (empty($uname) || empty($givenname) || empty($surname) || empty($pwd)) {
      $errors[] = "Feltet kan ikke være tomt.";
    }
    return $errors;
  }
  
  // This functionallity could've been done in the user class as well - duplicate method - REMOVE METHOD?
  private function addAdmin ($uname, $pwd, $givenname, $surname) {
    // Validate before executing code    
    $count=count($this->validateAdmin($uname, $givenname, $surname, $pwd));
    
    if ($count>0) {
      // Insert error messages in HTML
      for ($i = 0; $i < $count; $i++ ) {
        echo $this->validateAdmin($uname, $givenname, $surname, $pwd)[$i];
      }
      return;
    }
    
    $sql = 'INSERT INTO users (email, password, givenname, surname, user_type) VALUES (?, ?, ?, ?, ?)';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($uname, password_hash($pwd, PASSWORD_DEFAULT), $givenname, $surname, 1));
    
    if ($sth->rowCount()==0) {
      $this->adminMessage = '<div class="alert alert-danger" role="alert">Feil oppstod!</div>';
    }

    // Success message
    $this->adminMessage = '<div class="success">Administrator suksessfullt opprettet!</div>';
  }
  
  // The method removes a single user
  private function deleteUser ($id) {
    
    // The select statement should get thumbnail and caption data as well
    $sql1  = "SELECT DISTINCT source, thumbnail FROM videos WHERE uid=?";
    $sth1 = $this->db->prepare($sql1);
    $sth1->execute(array($id));
    
    // Remove videos from folder
    while ($row = $sth1->fetch(PDO::FETCH_ASSOC)) {
      unlink("thumbnail/" . $row['thumbnail']);
      unlink("videos/" . $row['source']);
    }
    
    $sql2 = "DELETE FROM users WHERE id=?";
    $sth2 = $this->db->prepare($sql2);
    $sth2->execute(array($id));
  }
  
  // The method removes all users the admin has checked of
  private function deleteCheckedUsers ($numCheckBox) {
    for ($i = 0; $i < count($numCheckBox); $i++) {
      $userID = $numCheckBox[$i];
      
      $sql1 = "SELECT DISTINCT source FROM videos WHERE uid=?";
      $sth1 = $this->db->prepare($sql1);
      $sth1->execute(array($userID));
      
      while($row = $sth1->fetch(PDO::FETCH_ASSOC)) {
        unlink("videos/" . $row['source']);
      }
      
      $sql2 = "DELETE FROM users WHERE id=?";
      $sth2 = $this->db->prepare($sql2);
      $sth2->execute(array($userID));
      
    }
  }
  
  private function promoteUser ($id) {
    $sql = "UPDATE users SET user_type=? WHERE id=?";
    $sth = $this->db->prepare($sql);
    $sth->execute(array(1, $id));
  }
  
  private function getUser ($user_type) {
    $sql = 'SELECT id, email, givenname, surname FROM users WHERE user_type=?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($user_type));
    return $sth->fetchAll(PDO::FETCH_ASSOC);
  }
  
  // Check if user return 0 results
  private function completeUserInfo () {
    $query = "SELECT users.id, users.givenname, users.surname, COUNT(videos.uId) AS totalVideos, COUNT(playlists.uId) AS totalPlaylists FROM users LEFT JOIN videos ON users.id=videos.uId LEFT JOIN playlists ON users.id=playlists.uId WHERE users.user_type=1 GROUP BY users.id";
    $sth = $this->db->prepare($query);
    $sth->execute(array());
    
    return $sth->fetchAll(PDO::FETCH_ASSOC);
  }
  
  // Sort users functionallity
  private function sortUsersByName() {
    $query = "SELECT users.id, users.givenname, users.surname, COUNT(videos.uId) AS totalVideos, COUNT(playlists.uId) AS totalPlaylists FROM users LEFT JOIN videos ON users.id=videos.uId LEFT JOIN playlists ON users.id=playlists.uId WHERE users.user_type=1 GROUP BY users.id ORDER BY users.givenname";
    $sth = $this->db->prepare($query);
    $sth->execute(array());
    
    return $sth->fetchAll(PDO::FETCH_ASSOC);
  }
  
  private function sortByVideos() {
    $query = "SELECT users.id, users.givenname, users.surname, COUNT(videos.uId) AS totalVideos, COUNT(playlists.uId) AS totalPlaylists FROM users LEFT JOIN videos ON users.id=videos.uId LEFT JOIN playlists ON users.id=playlists.uId WHERE users.user_type=1 GROUP BY users.id ORDER BY totalVideos DESC";
    $sth = $this->db->prepare($query);
    $sth->execute(array());
    
    return $sth->fetchAll(PDO::FETCH_ASSOC);
  }
  
  private function sortByPlaylists() {
    // check if a user exists
    $query = "SELECT users.id, users.givenname, users.surname, COUNT(videos.uId) AS totalVideos, COUNT(playlists.uId) AS totalPlaylists FROM users LEFT JOIN videos ON users.id=videos.uId LEFT JOIN playlists ON users.id=playlists.uId WHERE users.user_type=1 GROUP BY users.id ORDER BY totalPlaylists DESC";
    $sth = $this->db->prepare($query);
    $sth->execute(array());
    
    return $sth->fetchAll(PDO::FETCH_ASSOC);
  }
  
}

$admin = new Admin($db);

?>