<!-- Form for å laste opp nye video-->
<form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" enctype="multipart/form-data">
  <div class="form-group">
      <label for="title">Tittel</label>
      <input type="text" class="form-control" name="title" id="title" placeholder="Tittel på videoen" value="<?php echo $this->getVideoInfo()['title']; ?>">
  </div>
  <div class="form-group">
      <label for="description">Beskrivelse</label>
      <textarea class="form-control" name="description" style="height: 150px;" placeholder="Beskrivelse av hva videoen inneholder"><?php echo $this->getVideoInfo()['description']; ?></textarea>
  </div>

  <div class="form-group form-inline">
   <label for="Video fil">Legg ved video:</label><br />
    <div class="fileUpload btn btn-primary">
        <span>Velg fil og last opp</span>
        <input type="file" name="file" id="file" class="upload" size="20" />
    </div>
    <input id="uploadFile" class="form-control" placeholder="Velg fil" name="video" disabled="disabled" />
  </div>

  <div class="form-group form-inline">
    <label for="velg thumbnail">Thumbnail:</label>
    <input type="file" name="thumbnail">
  </div>

  <div class="form-group">
      <label for="caption files">Språk filer (valgfritt):</label><br />
      <div class="row">
       <div class="col-sm-3">
         <input name="captionFile[]" type="file" />
       </div>
       <div class="col-sm-8">
         <select name="lang[]" id="lang1">
          <option value="none">Ingen</option>
          <option value="English">Engelsk</option>
          <option value="Norwegian">Norsk</option>
          <option value="Spanish">Spansk</option>
        </select>
       </div>
     </div>
     <div class="row">
       <div class="col-sm-3">
         <input name="captionFile[]" type="file" />
       </div>
       <div class="col-sm-8">
         <select name="lang[]" id="lang2">
          <option value="none">Ingen</option>
          <option value="English">Engelsk</option>
          <option value="Norwegian">Norsk</option>
          <option value="Spanish">Spansk</option>
        </select>
       </div>
     </div>
  </div>

  <div class="form-group">
     <div class="row">
       <div class="col-sm-offset-5 col-sm-5">
      <input class="btn btn-success" name="submit" id="submitbutton" type="submit" value="Lagre video"/>
      <input class="btn btn-danger" name="cancel" type="submit" value="Cancel"/>
    </div>
     </div>
  </div>
</form>
<?php
  // If exists, show error messages
  if (!empty($this->getFileErrors())) {
    echo "<div class='alert alert-danger'>";
      echo "<ul>";
      for ($i = 0; $i < count($this->fileErrors); $i++) {
        echo "<li>".$this->fileErrors[$i] . "</li>";
      }
      echo "</ul>";
    echo "</div>";
  }
?>
