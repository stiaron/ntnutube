<!-- A form used by the user when they create new playlist -->
<form id="playlistform" role="form" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
  <div class="form-group">
    <label for="title">Tittel</label>
    <input type="text" class="form-control" name="addTitle" id="title" placeholder="Tittel på spillelisten" value="<?php echo isset($_POST['addTitle']) ? $_POST['addTitle'] : ""; ?>">
  </div>
  <div class="form-group">
    <label for="title">Beskrivelse</label>
    <textarea class="form-control" name="addDescription" style="height: 150px;" placeholder="Beskrivelse av spillelisten"><?php echo isset($_POST['addDescription']) ? $_POST['addDescription'] : ""; ?></textarea>
  </div>
  <div style="margin-top:10px" class="form-group">
   <div class="row">
     <div class="col-sm-12">
      <input type="submit" id="btn-submit" class="btn btn-success" value="Create playlist"/>
    </div>
   </div>
  </div>
</form>

<?php
// If exists, show error messages
if (!empty($this->getPlaylistErrors())) {
  echo "<div class='alert alert-danger'>";
    echo "<ul>";
    for ($i = 0; $i < count($this->errors); $i++) {
      echo "<li>".$this->errors[$i] . "</li>";
    }
    echo "</ul>";
  echo "</div>";
} 
?>
