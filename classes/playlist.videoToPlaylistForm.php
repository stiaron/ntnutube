<?php
if (count($this->getPlaylists($this->uId)) === 0)
{
  echo "<h4>Du må lage en spilleliste.</h4>";
  echo "<a href='createPlaylist.php'>Klikk her</a>";
}
else
{ ?>
  <form  method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" >
  <div class="form-group">
    <label for="My playlist(s)"></label>Mine spillelister:<br><br>
    <select class="form-control" id="add" name="playlistID">
      <?php
        foreach($this->getPlaylists($this->uId) as $playlist)
          echo "<option value='" . $playlist['pId'] ."'>" . $playlist['title'] . "</option>";
       ?>
    </select>
   </div>
    <div class="form-group">
     <input type="submit" id="add" class="btn btn-default" value="Add To Playlist" />
    </div>
  </form>
<?php
}?>
