<!-- Henter ut formen for å kunne gi vanlig bruker admin rettigheter-->
 <form method="post" class="form-horizontal" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
   <div class="form-group">
    <div class="col-sm-12">
      <select class="form-control" id="sel1" name="userToPromote">
        <?php
        foreach($members as $member) { ?>
          <option value="<?php echo $member['id']; ?>"><?php echo $member['givenname'] . " " . $member['surname']; ?></option>
        <?php } ?>
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-12">
      <input type="submit" id="promoteUSer" class="btn btn-default" value="Promote" />
    </div>
  </div>
</form>
