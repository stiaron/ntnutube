<?php
session_start();
require_once 'includes/db.php';
require_once 'classes/admin.php';
require_once 'classes/user.php';
require_once 'classes/playlist.php';

$pageTitle = "Administrer spilleliste";

require_once 'includes/heading.php';
?>
<div class="container">
<?php
if ($user->isLoggedIn()) {
  if (isset($_GET['videoID'])) {
	?>
  	<div class="mainbox col-md-6 col-sm-offset-3">
    	<div class="panel panel-default">
     		<div class="panel-heading">
      		<h4>Add Video</h4>
      	</div>
        <div class="panel-body">
           <?php
            // Check if the user has created a playlist, and show message otherwise
            $playlist->insertAddVideoForm();
            ?>
        </div>
      </div>
    </div>
      <?php
	}
	else { ?>
     <div class="mainbox col-md-6 col-sm-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          Velg en spilleliste å endre
        </div>
      	<div class="panel-body">
  <?php 
   if (count($playlist->getPlaylists($user->getUID())) > 0) { 
      $playlist->insertEditPlaylistForm(); //inserts the form  where the user selects which playlist to edit >
	} else {
        echo "<h4>Du må opprette en spilleliste.</h4>";
        echo "<a href='createPlaylist.php'>Klikk her</a>";
     } ?>
       </div>
      </div>
    </div>
    <?php 
    if(isset($_GET["chosenID"])) { ?>
    <div class="mainbox col-md-6 col-sm-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          Rediger spilleliste
        </div>
        <div class="panel-body">
          <?php $playlist->insertDypereEditPlaylist(); // inserts a form where the user can edit the playlist ?>
        </div>
      </div>
      <?php $playlist->validationErrors() ?>
    </div>
    <div class="mainbox col-md-6 col-sm-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          Endre video rekkefølge
        </div>
        <div class="panel-body">
          <?php $playlist->insertEditOrderForm(); // Change the order of videos in playlist ?>
        </div>
        
      </div>
      <?php if (isset($playlist->orderErrorMsg)) { ?>
        <div class="alert alert-danger">
          <strong>Fare!</strong>  <?php echo $playlist->orderErrorMsg ?>
        </div>
        <?php } ?>
    </div>
  <?php }
    }
} ?>
</div>
<?php require_once 'includes/footer.php'; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
