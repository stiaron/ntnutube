<?php

require_once('../db_info.php');

class Database {
  private $pdo;
  
  public function __construct($dbUser, $dbPwd) {
    
    $this->pdo = new PDO ('mysql:host=127.0.0.1', $dbUser, $dbPwd);
    $this->pdo->setAttribute(PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    
    $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'ntnutube'";
    
    $dbExists = (bool) $this->pdo->query($query)->fetchColumn();
    
    if (!$dbExists) {
      // Create database if not exists
      $this->pdo->query('CREATE DATABASE ntnutube');
      $this->pdo->query('USE ntnutube');
      
      $db_sql = file_get_contents(__DIR__ . '/../sql/database.sql');
      $this->pdo->exec($db_sql);
    } else {
      $this->pdo->query('USE ntnutube');
    }
  }
}

// Generate database with specified params
$my_db = new Database($dbUser, $dbPwd);

echo "Database succesfully created!";
?>