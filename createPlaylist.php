<?php
session_start();                                        // Session start
require_once 'includes/db.php';                         // Hvis included
require_once 'classes/admin.php';                       // Hvis included
require_once 'classes/user.php';                        // Hvis included
require_once 'classes/playlist.php';                    // Hvis included
$pageTitle = "Please create a playlist";
require_once 'includes/heading.php';                    // Hvis included

// This will allow an user to create a playlist.
if (isset($_POST['addTitle'])) {
  
  if($playlistValidation=$playlist->validatePlaylist($_POST['addTitle'],$_POST['addDescription'])==0) { //adds the playlist
  $res=$playlist->addPlaylist($_POST['addTitle'], $_POST['addDescription']);
      // Tilbake til fremside
      header("location:index.php?playlistID=".$playlist->lastPId);
    }
} ?>
<div class="mainbox col-md-6 col-sm-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          Opprett spilleliste
        </div>
        <div class="panel-body">
          <?php echo $playlist->insertPlaylistForm(); //inserts the playlist form ?>
        </div>
  </div>
</div>
<?php require_once 'includes/footer.php'; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>