<?php
session_start();								// Start the session
require_once 'includes/db.php';                 // Connect to the database
require_once 'classes/user.php';			    // Require admin class
require_once 'classes/admin.php';               // Require admin class if not alredy
require_once 'classes/video.php';               // Require video class if not alredy
require_once 'classes/playlist.php';            // Require playlist if not alredy

$pageTitle = "Velkommen til NTNUTube";

require_once 'includes/heading.php';
?>

<style>

  .controls {
	background: #e5e5e5;
    color: #000;
  }

  video {
    height: 100%;
    max-height: 500px;
    width: 100%;
    height: auto;
  }

  /* Container boxes */
  #infoContainer {
    padding: 15px;
    width: 100%;
    background: #f2f2f2;
  }

  section {
    padding: 15px;
    background: #f2f2f2;
    margin-bottom: 30px;
  }

  #captionBox {
    height: 500px;
    overflow: auto;
    border: 2px solid #000;
    padding: 20px;
  }

  #playlistBox {
      height: 250px;
    overflow: auto;
    padding: 20px;
  }

/* Style cue */
  .active {
    background: yellow;
  }

  .thumbnails {
    width: 100%;
  }

  #display > p:hover {
    cursor: pointer;
  }

</style>

<div class="container">
<?php
if (isset($_GET['v'])) {
  $arr = $video->getVideo($_GET['v']);
  $subtitles = $video->getCaptions($_GET['v']);
  $videoAvg = ($video->getAverageRating($_GET['v']) > 0) ? "Video gjennomsnitt: ". round($video->getAverageRating($_GET['v']))."/5" : "Ingen rating.";
?>

<div class="row">
  <div class="col-md-8">
   <h2><?php echo $arr['title']; ?></h2>
    <span id="subtitleMenu"></span>
    <video id="video" preload="metadata" controls poster="thumbnail/<?php echo $arr['thumbnail']; ?>">
    <source src="videos/<?php echo $arr['source']; ?>" />
    <?php for ($i=0; $i < count($subtitles); $i++)
      { ?>
        <track label="<?php echo $subtitles[$i]['language']; ?>" kind="subtitles" srclang="<?php echo strtolower(substr($subtitles[$i]['language'], 0, 2)); ?>" src="caption.php?captionFile=<?php echo urlencode($subtitles[$i]['source']); ?>">
<?php } ?>
      <!-- Loop over track files here -->
      HTML5 video not supported
    </video>
    <div class="controls" id="videoControl">
      <button id="playthrough" class="btn btn-default" type="button">2x</button>
    <?php for ($j = 0; $j < count($subtitles); $j++)
            echo "<button class='btn btn-default' onClick='changeSubtitle(".$j.")'>".$subtitles[$j]['language']."</button>";?>

          <a href="managePlaylist.php?videoID=<?php echo $_GET['v']; ?>" class="pull-right">
            <?php echo ($user->isLoggedIn()) ? "<button class='btn btn-default'>Add Video</button>" : ""; ?>
          </a>
    </div>
    <div id="infoContainer">
      <?php echo $arr['ts']; ?>

      <!-- User options -->
      <?php if ($user->isLoggedIn()) { ?>
        <div class="pull-right">
          <a href="<?php echo $_SERVER['REQUEST_URI']. '&rating=1'; ?>"> 1 </a>
          <a href="<?php echo $_SERVER['REQUEST_URI']. '&rating=2'; ?>"> 2 </a>
          <a href="<?php echo $_SERVER['REQUEST_URI']. '&rating=3'; ?>"> 3 </a>
          <a href="<?php echo $_SERVER['REQUEST_URI']. '&rating=4'; ?>"> 4 </a>
          <a href="<?php echo $_SERVER['REQUEST_URI']. '&rating=5'; ?>"> 5 </a>
        </div>
        <?php } ?>
        <p><?php echo $videoAvg ?></p>
        <div class="pull-right">
          <?php if (isset($_GET['success']))
            echo "<br>" . "Suksess! din rating er " . $_GET['rating'];?>
        </div>
    </div>
    <section>
      <?php echo $arr['description']; ?>
    </section>
  </div>
  <div class="col-md-4">
    <div id="captionBox">
      <div id="display"></div>
    </div>
<?php
}

if (isset($_GET['list']))
{
  $videos = $playlist->getVideosInPlaylistSorted($_GET['list']);
?>
  <br>
  <div class="panel panel-default">
    <div class="panel-heading">
      Videor i spilleliste
    </div>
    <div id="playlistBox" class="panel-body">

    <?php
    foreach ($videos as $myVid)
    {
      echo "<div class='row'>";
        echo "<a href='watch.php?v=".$myVid['vId']."&list=".$myVid['pId']."'>";
        echo "<div class='col-md-8'>";
        echo "<img class='thumbnails' src='thumbnail/".$myVid['thumbnail']."'>";
      echo "</div>";
      echo "<div class='col-md-4'>";
        echo "<h4>".$myVid['title']."</h4>";
      echo "</div>";
        echo "</a>";
      echo "</div>";
      echo "<hr>";
    } ?>
    </div>
  </div>
  </div>
</div>
<?php
}
?>
</div>
<?php require_once 'includes/footer.php'; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script>

// ---------------------
// Video controls logic
// ---------------------

$( document ).ready( function()
{ // Get a reference to video and playthrough button
  var video = document.getElementById('video');
  var playThroughBtn = document.getElementById('playthrough');
  
  // Change content of button (useful if multiple buttons are needed)
  function changeBtn (btn, value)
  {
      btn.innerHTML = value;
  }

  var isSpeeding = false;

  function toggleSpeed ()
  {
    if (isSpeeding)
    {
      video.playbackRate = 1.0;
      video.play();
      changeBtn(playThroughBtn, "2x");
      isSpeeding = false;
    }
    else
    {
        video.playbackRate = 2.0;
        video.play();
        changeBtn(playThroughBtn, "Normal");
        isSpeeding = true;
    }
  }
  // Click event to playthrough button
  playThroughBtn.addEventListener('click', function ()
  {
      toggleSpeed();
  });

  video.addEventListener('click', function ()
  {
      console.log("Video clicked!");
  })

}); // end - document ready

// -------------
// Caption Logic
// -------------

function readContent (htmlTrack)
{
  var myTrack = htmlTrack.track;
  var selector = $('#display');

  for (var i=0; i < myTrack.cues.length; i++)
  {
    var cue = myTrack.cues[i]; // Get specific cue from tracklist
    
    // Tried to highlight the first cue when user changed subtitle file
    /*if (arguments.callee.caller.name === "changeSubtitle") {
      if (i == 0) {
        // Change the first subtitle track to active
        $('p[data-startTime=0]').addClass('active');
      }
    }*/
    cue.onenter = function()
    { // Add class
      $('p[data-startTime="'+this.startTime+'"]').addClass('active');
    }

    cue.onexit = function ()
    { // Remove class
      $('p[data-startTime="'+this.startTime+'"]').removeClass('active');
    }

    selector.append("<p data-startTime='"+cue.startTime+"'>"+cue.text+"</p>");
  }

  $('#display p').click(function ()
  {
      video.currentTime = $(this).attr('data-startTime');
  });
}

// ----------------------------------------------
// Caption Text Logic
// ----------------------------------------------

// Hide and load captions on video
for (var i = 0; i < video.textTracks.length; i++)
  video.textTracks[i].mode = 'hidden';

var disp = document.getElementById('display'); // Get tracks as HTML elements
var htmlTracks = document.querySelectorAll('track');

function changeSubtitle(index)
{
  $('#display p').remove(); // Remove prev. appended elements

  if (htmlTracks[index].readyState == 2)
    readContent(htmlTracks[index]);
  else                                         
  {
    // will force the track to be loaded
    htmlTracks[index].addEventListener('load', function (e)
    {
      readContent(htmlTracks[index]);
    });
  }
}
// Load first textTrack as default
document.addEventListener("DOMContentLoaded", function ()
{
  if (htmlTracks[0].readyState == 2)
    readContent(htmlTracks[0]);
  else 
  {
    // will force the track to be loaded
    htmlTracks[0].addEventListener('load', function (e)
    {
      readContent(htmlTracks[0]);
    });
  }
});

</script>
</body>
</html>
