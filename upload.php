<?php
session_start();                                    // Start Session
require_once 'includes/db.php';                     // Connect to database
require_once 'classes/user.php';                    // require user class
require_once 'classes/video.php';                   // require vide class

$pageTitle = "Please upload a video";

require_once 'includes/heading.php';

?>
<div class="container">
  <div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
	   <div class="panel-heading">
	    <h3 class="panel-title">Last opp en video</h3>
	   </div>
	   <div class="panel-body" style="margin-top: 10px;">
	    <?php echo $video->insertNewVideoForm(); ?>
		 </div>
	  </div>
  </div>
</div>

<?php
require_once 'includes/footer.php'; // include footer
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<script>
$('#file').on('change', function (e)
{
  if (e.target.files.length==1)
    $('#uploadFile').val($('#file').val().split('\\').pop());
});

// Keep lang. values after form submit
document.getElementById('lang1').value = "<?php  echo isset($_POST['lang'][0]) ? $_POST['lang'][0] : 'none';  ?>";
document.getElementById('lang2').value = "<?php  echo isset($_POST['lang'][1]) ? $_POST['lang'][1] : 'none';  ?>";
</script>
</body>
</html>
