-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02. Mar, 2017 16:44
-- Server-versjon: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ntnutube`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `persistent_login`
--

CREATE TABLE `persistent_login` (
  `uid` bigint(20) NOT NULL,
  `series` char(255) COLLATE utf8_bin NOT NULL,
  `token` char(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `playlists`
--

CREATE TABLE `playlists` (
  `pId` bigint(20) NOT NULL,
  `uId` bigint(20) NOT NULL,
  `title` varchar(128) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `play_video`
--

CREATE TABLE `play_video` (
  `vId` bigint(20) NOT NULL,
  `pId` bigint(20) NOT NULL,
  `position` int(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `rating`
--

CREATE TABLE `rating` (
  `uId` bigint(20) NOT NULL,
  `vId` bigint(20) NOT NULL,
  `rating` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `transcripts`
--

CREATE TABLE `transcripts` (
  `tId` bigint(20) NOT NULL,
  `vId` bigint(20) DEFAULT NULL,
  `language` varchar(128) COLLATE utf8_bin NOT NULL,
  `source` varchar(128) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `email` varchar(128) COLLATE utf8_bin NOT NULL,
  `password` char(255) COLLATE utf8_bin NOT NULL,
  `givenname` varchar(128) COLLATE utf8_bin NOT NULL,
  `surname` varchar(128) COLLATE utf8_bin NOT NULL,
  `user_type` enum('0','1') COLLATE utf8_bin NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dataark for tabell `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `givenname`, `surname`, `user_type`) VALUES
(12, 'mathisg@ntnu.no', '$2y$10$om5cJOTlXwlvRyZDBgvp6.P/271vl5c8V7Zjj/hMKqxr/ABIGBOK.', 'Mathis', 'Garberg', '1'),
(14, 'test@test.com', '$2y$10$JHC2GQ5CmL0AB99yCtvGe.F7eB.4xFFHhUc97v6gGhAygtpza.PRq', 'test', 'Testeren', '0');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `videos`
--

CREATE TABLE `videos` (
  `vId` bigint(20) NOT NULL,
  `uId` bigint(20) NOT NULL,
  `title` varchar(128) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `source` varchar(128) COLLATE utf8_bin NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `thumbnail` varchar(128) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `persistent_login`
--
ALTER TABLE `persistent_login`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `playlists`
--
ALTER TABLE `playlists`
  ADD PRIMARY KEY (`pId`,`uId`),
  ADD KEY `uName` (`uId`);

--
-- Indexes for table `play_video`
--
ALTER TABLE `play_video`
  ADD PRIMARY KEY (`vId`,`pId`),
  ADD KEY `delPlaylist` (`pId`),
  ADD KEY `position` (`position`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`uId`,`vId`),
  ADD KEY `Delvideo` (`vId`);

--
-- Indexes for table `transcripts`
--
ALTER TABLE `transcripts`
  ADD PRIMARY KEY (`tId`),
  ADD KEY `vId` (`vId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`,`email`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `email_2` (`email`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`vId`,`uId`),
  ADD KEY `uName` (`uId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `persistent_login`
--
ALTER TABLE `persistent_login`
  MODIFY `uid` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `playlists`
--
ALTER TABLE `playlists`
  MODIFY `pId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `play_video`
--
ALTER TABLE `play_video`
  MODIFY `position` int(120) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transcripts`
--
ALTER TABLE `transcripts`
  MODIFY `tId` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `vId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `playlists`
--
ALTER TABLE `playlists`
  ADD CONSTRAINT `users` FOREIGN KEY (`uId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `play_video`
--
ALTER TABLE `play_video`
  ADD CONSTRAINT `delPlaylist` FOREIGN KEY (`pId`) REFERENCES `playlists` (`pId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `delVid` FOREIGN KEY (`vId`) REFERENCES `videos` (`vId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `rating`
--
ALTER TABLE `rating`
  ADD CONSTRAINT `Deluser` FOREIGN KEY (`uId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Delvideo` FOREIGN KEY (`vId`) REFERENCES `videos` (`vId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `transcripts`
--
ALTER TABLE `transcripts`
  ADD CONSTRAINT `delTranscript` FOREIGN KEY (`vId`) REFERENCES `videos` (`vId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `uservideo` FOREIGN KEY (`uId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
