<?php
session_start();                                        // Start the session
require_once 'includes/db.php';                         // Connect to database
require_once 'classes/user.php';                        // Require user class if not already exists

if ($user->isLoggedIn())                                // Show login content
  header("Location: index.php");

if (isset($_POST['firstname']))                         // Check if input is empty
{                                                       // This will allow a new user to register on the website.
  //checks if all forms have been filled inn.
  if($validation=$user->validateReg ($_POST['email'], $_POST['passwd'], $_POST['firstname'], $_POST['lastname'])==0)
  { //adds the user
    $res=$user->addUser($_POST['email'], $_POST['passwd'], $_POST['firstname'], $_POST['lastname']);
    if (isset($res['success']))
      header('Location: index.php');
    else
      $newUserError = true; // if the email already is registered, the user haft to enter a new one.
  }
  else // If a form is not filled in, return the user to signin so that they can try again.
    $newUserError = true;
}

$pageTitle = "Please register or sign up";
require_once 'includes/heading.php';

echo $user->insertLoginForm();
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
  <?php
  if (isset ($newUserError)) // The following was added by Øivind Kolloen
  { ?>
  <script type="text/javascript">
    $(function()
    {	// When document is loaded, hide the login and show signup
        $('#loginbox').hide(); $('#signupbox').show();
    });
  </script>
  <?php } ?>
  </body>
</html>
