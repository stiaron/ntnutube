 <?php
session_start();								// Start the session
require_once 'includes/db.php';					// Connect to the database
require_once 'classes/user.php';				// Do login stuff
require_once 'classes/admin.php';               // include admin classe
require_once 'classes/video.php';               // require video class if not already exists
require_once 'classes/playlist.php';            // require playlist class if not already exists

$pageTitle = "Velkommen til NTNUTube";        // Tittel for siden

require_once 'includes/heading.php';            // include heading
?>
      <div class="container">
        <div class="row">
          <!-- Show top 10 results for video and playlist search -->
          <?php
          if (!empty($_GET['q']))
          { ?>
            <div class="col-md-6">
              <?php $video->listVideoResults($_GET['q']); ?>
            </div>
            <div class="col-md-6">
              <?php $playlist->listPlaylistResults($_GET['q']); ?>
            </div>
      <?php
          } ?>
        </div>
      </div>
    <?php require_once 'includes/footer.php'; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
  </body>
</html>
