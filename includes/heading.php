<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $pageTitle ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="css/upload.css"/>
  </head>
  <body>
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <!-- Change from toggle button -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!-- Admin panel and videoupload -->
      <a href="index.php" class="navbar-left"><img class="logo" src="img/ntnutube_logo.png" alt="ntnutube logo" /></a>
      <?php if ($user->isAdmin()) echo '<a href="adminView.php" class="btn btn-default navbar-btn">Admin Oversikt</a>' ?>
      <?php if ($user->isLoggedIn()) echo '<a href="upload.php" class="btn btn-default navbar-btn">Last opp video</a>' ?>
      <?php if ($user->isLoggedIn()) echo '<a href="createPlaylist.php" class="btn btn-default navbar-btn">Opprett spilleliste</a>' ?>
        <?php if ($user->isLoggedIn()) echo '<a href="managePlaylist.php" class="btn btn-default navbar-btn">Rediger spilleliste</a>' ?>
    </div>

      <ul class="nav navbar-nav navbar-right">
		<li>
			<form action="search.php" method="get" class="navbar-form navbar-left" role="search">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Søk" name="q" value="<?php echo isset($_GET['q']) ? $_GET['q'] : ''; ?>"></input>
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
              </div>
	 	    </form>
		</li>
      	<?php
      		if ($user->isLoggedIn())
      			echo '<a href="index.php?logout=true" class="btn btn-default navbar-btn">Logg Ut</a>';
      		else
      			echo '<a href="signin.php" class="btn btn-default navbar-btn">Logg Inn</a>'
      	?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
