<?php
session_start();																// Start the session
require_once 'includes/db.php';									// Connect to the database
require_once 'classes/user.php';								// Do login stuff
require_once 'classes/admin.php';
require_once 'classes/video.php';
require_once 'classes/playlist.php';

$pageTitle = "Velkommen til NTNUTube";
require_once 'includes/heading.php';
?>

<style>
.row.alternate:nth-child(even) {
	background-color: #eee; /* Class row and alternate, all even rows will have this color */
}

.thumbnail {
  width: 100%;
}
</style>

<div class="container">
  <div class="row">
<!--  Is user logget in ? Yes -->
<?php if ($user->isLoggedIn()) { ?>												
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">Mine Videoer</div>
        <div class="panel-body">
        <?php //Loop through all of the videos
          foreach ($video->insertMyVideos($user->getUID()) as $newVideo) { ?>

         <div class="row <?php
            // Check which video was just uploaded
            if (isset($_GET['videoID']) && $newVideo['vId'] == $_GET['videoID']) {
              echo "bg-success";
            } else {
              echo "alternate";
            }
          ?>">
          <div class="col-xs-9">
              <h3><a href="<?php echo "watch.php?v=" . $newVideo['vId']; ?>"><?php echo $newVideo['title']; ?></h3>
             <img class="thumbnail" src="thumbnail/<?php echo $newVideo['thumbnail']; ?>">
            <p><?php echo $newVideo['ts']; ?></p>
            </a>
          </div>
         </div>
      <?php } ?>
        </div>
        </div>
      </div>
      <div class="col-md-4">
       <div class="panel panel-default">
          <div class="panel-heading">Spillelister</div>
          <div class="panel-body">
            <?php //Loop through all of the  playlists
              foreach ($playlist->getPlaylists($user->getUID()) as $listOfPlays) { ?>

             <div class="row <?php
                // Check which playlist was just uploaded

                if (isset($_GET['playlistID']) && ($listOfPlays['pId'] == $_GET['playlistID']) ) {
                  echo "bg-success";
                } else {
                  echo "alternate";
                }
                ?>">
              <div class="col-xs-9">
               <?php if (isset($listOfPlays['vId'])) { ?>
                  <h3><a href="<?php echo "watch.php?v=".$listOfPlays['vId']."&list=" . $listOfPlays['pId']; ?>"><?php echo $listOfPlays['title']; ?></a></h3>
                  <p><?php echo $listOfPlays['description']; ?></p>
                  <p><?php echo $listOfPlays['ts']; ?></p>
                <?php } else { ?>
                  <h3><?php echo $listOfPlays['title']; ?> (Empty)</h3>
                  <p><?php echo $listOfPlays['description']; ?></p>
                  <p><?php echo $listOfPlays['ts']; ?></p>
                <?php } ?>
              </div>
             </div>
          <?php } ?>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">Populære Videoer</div>
          <div class="panel-body">
            <?php foreach ($video->getPopVid() as $popVid) { ?>
              <div class="row alternate">
                <div class="col-xs-9">
                  <a href="<?php echo "watch.php?v=" . $popVid['vId']; ?>"><h3><?php echo $popVid['title']; ?></h3>
                    <img class="thumbnail" src="thumbnail/<?php echo $popVid['thumbnail']; ?>">
                    <p><?php echo $popVid['ts']; ?></p>
                  </a>
                </div>
               </div>
            <?php } ?>
          </div>
        </div>
      </div>
<?php } else { ?>
<div class="col-md-4">
  <div class="panel panel-default">
    <div class="panel-heading">Nye Videoer</div>
    <div class="panel-body">
      <?php foreach ($video->insertNewVideos() as $newVideo) { ?>
      <div class="row alternate">
          <div class="col-xs-9">
            <a href="<?php echo "watch.php?v=" . $newVideo['vId']; ?>"><h3><?php echo $newVideo['title']; ?></h3>
            <img class="thumbnail" src="thumbnail/<?php echo $newVideo['thumbnail']; ?>">
            <p><?php echo $newVideo['ts']; ?></p>
            </a>
          </div>
         </div>
      <?php } ?>
    </div>
  </div>
</div>
<div class="col-md-4">
  <div class="panel panel-default">
    <div class="panel-heading">Offentlige Spillelister</div>
		<div class="panel-body">
			<?php //Loop through all of the  playlists
		    foreach ($playlist->getAllPlaylits() as $allLists) { ?>
			 <div class="row">
              <div class="col-xs-9">
                <?php if (isset($allLists['vId'])) { ?>
                  <h3><a href="<?php echo "watch.php?v=".$allLists['vId']."&list=" . $allLists['pId']; ?>"><?php echo $allLists['title']; ?></a></h3>
                  <p><?php echo $allLists['description']; ?></p>
                  <p><?php echo $allLists['ts']; ?></p>
                <?php } else { ?>
                  <h3><?php echo $allLists['title']; ?> (Empty)</h3>
                  <p><?php echo $allLists['description']; ?></p>
                  <p><?php echo $allLists['ts']; ?></p>
                <?php } ?>
             </div>
            </div>
		<?php } ?>
		</div>
	</div>
</div>
 <div class="col-md-4">
   <div class="panel panel-default">
     <div class="panel-heading">Populære Videoer</div>
     <div class="panel-body">
      <?php foreach ($video->getPopVid() as $popVid) { ?>
        <div class="row alternate">
          <div class="col-xs-9">
            <a href="<?php echo "watch.php?v=" . $popVid['vId']; ?>"><h3><?php echo $popVid['title']; ?></h3>
              <img class="thumbnail" src="thumbnail/<?php echo $popVid['thumbnail']; ?>">
              <p><?php echo $popVid['ts']; ?></p>
 	        </a>
          </div>
        </div>
      <?php } ?>
     </div>
   </div>
 </div>
  </div>
</div>
<?php } ?>
</div>
</div>
<?php require_once 'includes/footer.php'; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
