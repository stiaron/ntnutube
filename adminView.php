<?php
session_start();																			// Start the session
require_once 'includes/db.php';												// Connect to the database
require_once 'classes/user.php';											// Do login stuff
require_once 'classes/admin.php'; 										// Stuff related to bookmarks

$pageTitle = "Administrator Oversikt";														// Tittlen
require_once 'includes/heading.php';									// include om ikke er fra før

?>

<div class="container">
<?php
if ($user->isLoggedIn())															// er innlogget ?
{
  if ($user->isAdmin())																// er admin ?
	{ ?>
    <div class='row'>
    	<div class='col-md-offset-1 col-md-5'>
      	<div class="panel panel-default">
        	<div class="panel-heading">Opprett Administrator</div>
          <div class="panel-body">
          	<?php $admin->insertAddAdminForm(); ?>
          </div>
        </div>
      </div>
      <div class='col-md-5'>
      	<div class="panel panel-default">
          <div class="panel-heading">Administrer Brukere</div>
          <div class="panel-body">
            <?php $admin->insertDeleteUserForm(); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
    	<div class="col-md-offset-1 col-md-5">
      	<div class="panel panel-default">
          <div class="panel-heading">Forfrem Bruker</div>
          <div class="panel-body">
          	<?php $admin->insertPromoteForm(); ?>
          </div>
        </div>
      </div>
      <div class="col-md-5">
        <div class="panel panel-default">
          <div class="panel-heading">Admin Oversikt</div>
          <div class="panel-body">
            <?php $admin->insertAdminList(); ?>
          </div>
        </div>
      </div>
    </div>
<?php
  }
	else 																									// er ikke admin
    echo "Du har ikke tilgang til denne siden.";
}
else 																										// er ikke innlogget
  header ("Location: signin.php");
?>
</div>

<?php
require_once 'includes/footer.php';
?>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
		<script>
          $("#checkAll").change(function ()												
          {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
          });
		</script>
	</body>
</html>
